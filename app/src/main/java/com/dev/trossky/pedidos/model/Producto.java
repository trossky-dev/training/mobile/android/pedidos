package com.dev.trossky.pedidos.model;

/**
 * Created by luis on 30/09/17.
 */

public class Producto {
    public String idProducto;
    public String nombre;
    public Float precio;
    public Integer existencias;

    public Producto(String idProducto, String nombre, Float precio, Integer existencias) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.precio = precio;
        this.existencias = existencias;
    }
}
