package com.dev.trossky.pedidos.sqlite;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.provider.BaseColumns;

import com.dev.trossky.pedidos.sqlite.ContratoPedidos.*;


/**
 * Clase que administra la conexión de la base de datos y su estructuración
 */
public class BaseDatosPedidos extends SQLiteOpenHelper {

    private static final String NOMBRE_BASE_DATOS="pedidos.db";
    private static final Integer VERSION_ACTUAL=1;

    private final Context contexto;

    interface Tablas{
        String CABECERA_PEDIDO="cabecera_pedido";
        String DETALLE_PEDIDO="detalle_pedido";
        String PRODUCTO="producto";
        String CLIENTE="cliente";
        String FORMA_PAGO="forma_pago";

    }

    interface Referencias{
        String ID_CABECERA_PEDIDO=String.format("REFERENCES %s (%s) ON DELETE CASCADE",
                Tablas.CABECERA_PEDIDO, ContratoPedidos.CabeceraPedido.ID);
        String ID_PROUCTO=String.format("REFERENCES %s (%s) ",
                Tablas.PRODUCTO, ContratoPedidos.Producto.ID);
        String ID_CLIENTE=String.format("REFERENCES %s (%s)",
                Tablas.CLIENTE, ContratoPedidos.Cliente.ID);
        String ID_FORMA_PAGO=String.format("REFERENCES %s (%s)",
                Tablas.FORMA_PAGO, ContratoPedidos.FormasDePago.ID);
    }

    public BaseDatosPedidos( Context contexto) {
        super(contexto, NOMBRE_BASE_DATOS, null, VERSION_ACTUAL);
        this.contexto = contexto;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        if (!db.isReadOnly()){
            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN){

                db.setForeignKeyConstraintsEnabled(true);
            }else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
        "%s TEXT UNIQUE NOT NULL, %s DATETIME NOT NULL, %s TEXT NOT NULL %s, "+
        "%s TEXT NOT NULL %s) ",
                Tablas.CABECERA_PEDIDO, BaseColumns._ID,
                CabeceraPedido.ID,CabeceraPedido.FECHA,
                CabeceraPedido.ID_CLIENTE,Referencias.ID_CLIENTE,
                CabeceraPedido.ID_FORMA_PAGO, Referencias.ID_FORMA_PAGO ));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "%s TEXT  NOT NULL %s, %s INTEGER  NOT NULL CHECK (%s >0), " +
                        "%s INTEGER NOT NULL %s, "+
                        "%s INTEGER NOT NULL, " +
                        "%s REAL NOT NULL, UNIQUE(%s,%s) ) ",
                Tablas.DETALLE_PEDIDO, BaseColumns._ID,
                DetallePedido.ID,Referencias.ID_CABECERA_PEDIDO,
                DetallePedido.SECUENCIA,DetallePedido.SECUENCIA,
                DetallePedido.ID_PRODUCTO,Referencias.ID_PROUCTO,
                DetallePedido.CANTIDAD, DetallePedido.PRECIO,
                DetallePedido.ID,DetallePedido.SECUENCIA));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "%s TEXT NOT NULL UNIQUE, %s TEXT NOT NULL, " +
                        "%s REAL NOT NULL , "+
                        "%s INTEGER NOT NULL CHECK(%S>=0) ) ",
                Tablas.PRODUCTO, BaseColumns._ID,
                Producto.ID,Producto.NOMBRE,
                Producto.PRECIO,
                Producto.EXISTENCIAS,Producto.EXISTENCIAS ));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "%s TEXT  NOT NULL UNIQUE, %s TEXT NOT NULL, " +
                        "%s TEXT NOT NULL , "+
                        "%s  ) ",
                Tablas.CLIENTE, BaseColumns._ID,
                Cliente.ID,Cliente.NOMBRES,
                Cliente.APELLLIDOS,Cliente.TELEFONO ));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "%s TEXT  NOT NULL UNIQUE, %s TEXT NOT NULL ) ",
                Tablas.FORMA_PAGO, BaseColumns._ID,
                FormasDePago.ID,FormasDePago.NOMBRE));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+Tablas.CABECERA_PEDIDO);
        db.execSQL("DROP TABLE IF EXISTS "+Tablas.DETALLE_PEDIDO);
        db.execSQL("DROP TABLE IF EXISTS "+Tablas.PRODUCTO);
        db.execSQL("DROP TABLE IF EXISTS "+Tablas.CLIENTE);
        db.execSQL("DROP TABLE IF EXISTS "+Tablas.FORMA_PAGO);

        onCreate(db);

    }
}

