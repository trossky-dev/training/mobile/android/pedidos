package com.dev.trossky.pedidos.sqlite;

/**
 * El propósito de esta es guardar todos los metadatos y componentes auxiliares para definir la
 * estructura general de la base de datos.
 */

import java.util.UUID;

/**
 * Clase que establece los nombres a usar en la base de datos
 */
public class ContratoPedidos {

    interface ColumnasCabeceraPedido{
        String ID="id";
        String FECHA="fecha";
        String ID_CLIENTE="id_cliente";
        String ID_FORMA_PAGO="id_forma_pago";
    }
    interface ColumnasDetallePedido{
        String ID="id";
        String SECUENCIA="secuencia";
        String ID_PRODUCTO="id_producto";
        String CANTIDAD="cantidad";
        String PRECIO="precio";
    }

    interface ColumnasProducto{
        String ID="id";
        String NOMBRE="nombre";
        String PRECIO="precio";
        String EXISTENCIAS="existencias";
    }

    interface ColumnasCliente{
        String ID="id";
        String NOMBRES="nombres";
        String APELLLIDOS="apellidos";
        String TELEFONO="telefono";
    }

    interface ColumnasFormasDePago{
        String ID="id";
        String NOMBRE="nombre";
    }

    public static class CabeceraPedido implements  ColumnasCabeceraPedido{
        public static String generarIdCabeceraPedido(){
            return "CP-"+ UUID.randomUUID().toString();
        }

    }

    public static class DetallePedido implements  ColumnasDetallePedido{
        //todo metodos auxiliares
    }

    public static class Producto implements  ColumnasProducto{
        public static String generarIdProducto(){
            return "PRO-"+ UUID.randomUUID().toString();
        }

    }
    public static class  Cliente implements  ColumnasCliente{
        public static String generarIdCliente(){
            return "CLI-"+ UUID.randomUUID().toString();
        }

    }
    public static class FormasDePago implements  ColumnasFormasDePago{
        public static String generarIdFormasDePago(){
            return "FP-"+ UUID.randomUUID().toString();
        }
    }

    private ContratoPedidos() {
    }
}
