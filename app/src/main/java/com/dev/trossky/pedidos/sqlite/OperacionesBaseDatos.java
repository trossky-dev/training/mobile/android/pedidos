package com.dev.trossky.pedidos.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import  com.dev.trossky.pedidos.sqlite.BaseDatosPedidos.*;
import com.dev.trossky.pedidos.sqlite.ContratoPedidos.*;
import com.dev.trossky.pedidos.sqlite.ContratoPedidos.CabeceraPedido;

/**
 * Clase auxiliar que implementa a {@link BaseDatosPedidos} para llevar a cabo el CRUD
 * sobre las entidades existentes.
 */

public class OperacionesBaseDatos {


    private static BaseDatosPedidos baseDatos;
    private static OperacionesBaseDatos instancia= new OperacionesBaseDatos();

    private OperacionesBaseDatos() {
    }

    public static OperacionesBaseDatos obtenerInstancia(Context contexto){
        if (baseDatos==null){
            baseDatos= new BaseDatosPedidos(contexto);
        }
        return instancia;
    }
    /** {@link ContratoPedidos.CabeceraPedido}
     * OPERACIONES
     * */
    //@GET ALL CabeceraPedido
    public Cursor obtenerCabeceraPedido(){
        SQLiteDatabase db =baseDatos.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO);


        return builder.query(db,proyCabeceraPedido,null,null,null,null,null);

    }

    private static final String CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO = "cabecera_pedido " +
            "INNER JOIN cliente " +
            "ON cabecera_pedido.id_cliente= cliente.id " +
            "INNER JOIN forma_pago " +
            "ON cabecera_pedido.id_forma_pago=forma_pago.id ";

    private final String[] proyCabeceraPedido= new String[]{
            Tablas.CABECERA_PEDIDO+"."+ CabeceraPedido.ID,
            CabeceraPedido.FECHA,
            ContratoPedidos.Cliente.NOMBRES,
            ContratoPedidos.Cliente.APELLLIDOS,
            FormasDePago.NOMBRE
    };

    //@GET BY ID Cabecera Pedido
    public Cursor obtenerCabeceraPedidoPorId(String id){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String selection=String.format("%s=?",CabeceraPedido.ID);
        String[] selectionArgs={id};

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO);

         String[] proyeccion= new String[]{
                Tablas.CABECERA_PEDIDO+"."+ CabeceraPedido.ID,
                CabeceraPedido.FECHA,
                ContratoPedidos.Cliente.NOMBRES,
                ContratoPedidos.Cliente.APELLLIDOS,
                FormasDePago.NOMBRE
        };


        return builder.query(db,proyeccion,selection,selectionArgs,null,null,null);
    }

    //@POST Cabecera Pedido
    public String insertatCabeceraPedido(com.dev.trossky.pedidos.model.CabeceraPedido pedido){

        SQLiteDatabase db=baseDatos.getWritableDatabase();

        //generate PK
        String idCabeceraPedido= CabeceraPedido.generarIdCabeceraPedido();

        ContentValues valores= new ContentValues();

        valores.put(CabeceraPedido.ID,idCabeceraPedido);
        valores.put(CabeceraPedido.FECHA,pedido.fecha);
        valores.put(CabeceraPedido.ID_CLIENTE,pedido.idCliente);
        valores.put(CabeceraPedido.ID_FORMA_PAGO, pedido.idFormaPago);

        // Insertar cabecera
        db.insertOrThrow(Tablas.CABECERA_PEDIDO,null,valores);

        return idCabeceraPedido;
    }

    //@PUT Cabecera Pedido
    public boolean actualizarCabeceraPedido(com.dev.trossky.pedidos.model.CabeceraPedido pedidoNuevo){


        SQLiteDatabase db= baseDatos.getWritableDatabase();

        ContentValues valores= new ContentValues();

        valores.put(CabeceraPedido.FECHA,pedidoNuevo.fecha);
        valores.put(CabeceraPedido.ID_CLIENTE,pedidoNuevo.idCliente);
        valores.put(CabeceraPedido.ID_FORMA_PAGO, pedidoNuevo.idFormaPago);

        String whereClause=String.format("%s=?",CabeceraPedido.ID);
        String[] whereArgs={pedidoNuevo.idCabeceraPedido};

        int resultado=db.update(Tablas.CABECERA_PEDIDO,valores,whereClause,whereArgs);

        return resultado>0;
    }

    //@DELETE Cabecera Pedido
    public boolean eliminarCabeceraPedido(String idCabeceraPedido){


        SQLiteDatabase db= baseDatos.getWritableDatabase();


        String whereClause=CabeceraPedido.ID+" =? ";
        String[] whereArgs={idCabeceraPedido};

        int resultado=db.delete(Tablas.CABECERA_PEDIDO,whereClause,whereArgs);

        return resultado>0;
    }




    /** {@link ContratoPedidos.DetallePedido}
     * OPERACIONES
     * */
    public Cursor obtenerDetallePedido() {
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s ",
                Tablas.DETALLE_PEDIDO);


        return db.rawQuery(sql,null);

    }

    //@GET BY idCabeceraPedido DetallePedido
    public Cursor obtenerDetallePedidoPorIdPedido(String idCabeceraPedido){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s WHERE %s=?",
                Tablas.DETALLE_PEDIDO, DetallePedido.ID);


        String[] selectionArgs={idCabeceraPedido};



        return db.rawQuery(sql,selectionArgs);
    }

    //@POST DetallePedido
    public String insertatDetallePedido(com.dev.trossky.pedidos.model.DetallePedido detalle){

        SQLiteDatabase db=baseDatos.getWritableDatabase();



        ContentValues valores= new ContentValues();

        valores.put(DetallePedido.ID,detalle.idCabeceraPedido);
        valores.put(DetallePedido.SECUENCIA,detalle.secuencia);
        valores.put(DetallePedido.ID_PRODUCTO,detalle.idProducto);
        valores.put(DetallePedido.CANTIDAD, detalle.cantidad);
        valores.put(DetallePedido.PRECIO, detalle.precio);



        // Insertar DetallePedido
        db.insertOrThrow(Tablas.DETALLE_PEDIDO,null,valores);

        return String.format("%s#%d",detalle.idCabeceraPedido,detalle.secuencia);
    }

    //@PUT DetallePedido
    public boolean actualizarDetallePedido(com.dev.trossky.pedidos.model.DetallePedido detalleNuevo){


        SQLiteDatabase db= baseDatos.getWritableDatabase();

        ContentValues valores= new ContentValues();

        valores.put(DetallePedido.SECUENCIA,detalleNuevo.secuencia);
        valores.put(DetallePedido.CANTIDAD,detalleNuevo.cantidad);
        valores.put(DetallePedido.PRECIO, detalleNuevo.precio);

        String whereClause=String.format("%s=? AND %s=? ",
                DetallePedido.ID, DetallePedido.SECUENCIA );
        String[] whereArgs={detalleNuevo.idCabeceraPedido, String.valueOf(detalleNuevo.secuencia)};

        int resultado=db.update(Tablas.DETALLE_PEDIDO,valores,whereClause,whereArgs);

        return resultado>0;
    }

    //@DELETE DetallePedido
    public boolean eliminarDetallePedido(String idCabeceraPedido, Integer secuencia){


        SQLiteDatabase db= baseDatos.getWritableDatabase();


        String whereClause=String.format("%s=? AND %s=? ",
                DetallePedido.ID, DetallePedido.SECUENCIA );
        String[] whereArgs={idCabeceraPedido, String.valueOf(secuencia)};

        int resultado=db.delete(Tablas.DETALLE_PEDIDO,whereClause,whereArgs);

        return resultado>0;
    }






    /** {@link ContratoPedidos.Producto}
     * OPERACIONES
     * */
    //@GET BY ALL Producto
    public Cursor obtenerProducto(){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s ",
                Tablas.PRODUCTO);

        return db.rawQuery(sql,null);
    }


    //@GET BY idProducto Producto
    public Cursor obtenerProductoIdProducto(String idProducto){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s WHERE %s=?",
                Tablas.PRODUCTO, Producto.ID);


        String[] selectionArgs={idProducto};



        return db.rawQuery(sql,selectionArgs);
    }

    //@POST Producto
    public String insertatProducto(com.dev.trossky.pedidos.model.Producto newProducto){

        SQLiteDatabase db=baseDatos.getWritableDatabase();

        // Generar PK
        String idProducto= Producto.generarIdProducto();


        ContentValues valores= new ContentValues();

        valores.put(Producto.ID,idProducto);
        valores.put(Producto.NOMBRE,newProducto.nombre);
        valores.put(Producto.PRECIO,newProducto.precio);
        valores.put(Producto.EXISTENCIAS, newProducto.existencias);




        // Insertar DetallePedido
        db.insertOrThrow(Tablas.PRODUCTO,null,valores);

        return idProducto;
    }

    //@PUT Producto
    public boolean actualizarProducto(com.dev.trossky.pedidos.model.Producto editProducto){


        SQLiteDatabase db= baseDatos.getWritableDatabase();

        ContentValues valores= new ContentValues();

        valores.put(Producto.NOMBRE,editProducto.nombre);
        valores.put(Producto.PRECIO,editProducto.precio);
        valores.put(Producto.EXISTENCIAS, editProducto.existencias);

        String whereClause=String.format("%s=? ",
                Producto.ID);
        String[] whereArgs={editProducto.idProducto};

        int resultado=db.update(Tablas.PRODUCTO,valores,whereClause,whereArgs);

        return resultado>0;
    }

    //@DELETE Producto
    public boolean eliminarProducto(String idProducto){


        SQLiteDatabase db= baseDatos.getWritableDatabase();


        String whereClause=String.format("%s=? ",
                Producto.ID);
        String[] whereArgs={idProducto};

        int resultado=db.delete(Tablas.PRODUCTO,whereClause,whereArgs);

        return resultado>0;
    }


    /** {@link ContratoPedidos.Cliente}
     * OPERACIONES
     * */
    //@GET BY ALL Cliente
    public Cursor obtenerCliente(){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s ",
                Tablas.CLIENTE);


        return db.rawQuery(sql,null);
    }


    //@GET BY idCliente Cliente
    public Cursor obtenerClienteIdCliente(String idCliente){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s WHERE %s=?",
                Tablas.CLIENTE, Cliente.ID);


        String[] selectionArgs={idCliente};



        return db.rawQuery(sql,selectionArgs);
    }

    //@POST Cliente
    public String insertatCliente(com.dev.trossky.pedidos.model.Cliente newCliente){

        SQLiteDatabase db=baseDatos.getWritableDatabase();

        // Generar PK
        String idCliente= Cliente.generarIdCliente();


        ContentValues valores= new ContentValues();

        valores.put(Cliente.ID,idCliente);
        valores.put(Cliente.NOMBRES,newCliente.nombres);
        valores.put(Cliente.APELLLIDOS,newCliente.apellidos);
        valores.put(Cliente.TELEFONO, newCliente.telefono);




        // Insertar DetallePedido
        db.insertOrThrow(Tablas.CLIENTE,null,valores);

        return idCliente;
    }

    //@PUT Cliente
    public boolean actualizarCliente(com.dev.trossky.pedidos.model.Cliente editCliente){


        SQLiteDatabase db= baseDatos.getWritableDatabase();

        ContentValues valores= new ContentValues();

        valores.put(Cliente.NOMBRES,editCliente.nombres);
        valores.put(Cliente.APELLLIDOS,editCliente.apellidos);
        valores.put(Cliente.TELEFONO, editCliente.telefono);

        String whereClause=String.format("%s=? ",
                Cliente.ID);
        String[] whereArgs={editCliente.idCliente};

        int resultado=db.update(Tablas.CLIENTE,valores,whereClause,whereArgs);

        return resultado>0;
    }

    //@DELETE Cliente
    public boolean eliminarCliente(String idCliente){


        SQLiteDatabase db= baseDatos.getWritableDatabase();


        String whereClause=String.format("%s=? ",
                Cliente.ID);
        String[] whereArgs={idCliente};

        int resultado=db.delete(Tablas.CLIENTE,whereClause,whereArgs);

        return resultado>0;
    }

    /** {@link ContratoPedidos.FormasDePago}
     * OPERACIONES
     * */
    //@GET BY ALL FormaPago
    public Cursor obtenerFormaPago(){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s ",
                Tablas.FORMA_PAGO);


        return db.rawQuery(sql,null);
    }


    //@GET BY idFormaPago FormaPago
    public Cursor obtenerFormaPagoIdFormaPago(String idFormaPago){
        SQLiteDatabase db =baseDatos.getReadableDatabase();

        String sql=String.format("SELECT * FROM %s WHERE %s=?",
                Tablas.FORMA_PAGO, FormasDePago.ID);


        String[] selectionArgs={idFormaPago};



        return db.rawQuery(sql,selectionArgs);
    }

    //@POST FormasDePago
    public String insertatFormasDePago(com.dev.trossky.pedidos.model.FormaPago newFormaPago){

        SQLiteDatabase db=baseDatos.getWritableDatabase();

        // Generar PK
        String idFormaPago= FormasDePago.generarIdFormasDePago();


        ContentValues valores= new ContentValues();

        valores.put(FormasDePago.ID,idFormaPago);
        valores.put(FormasDePago.NOMBRE,newFormaPago.nombre);





        // Insertar DetallePedido
        db.insertOrThrow(Tablas.FORMA_PAGO,null,valores);

        return idFormaPago;
    }

    //@PUT FormasDePago
    public boolean actualizarFormasDePago(com.dev.trossky.pedidos.model.FormaPago editFormasDePago){


        SQLiteDatabase db= baseDatos.getWritableDatabase();

        ContentValues valores= new ContentValues();

        valores.put(FormasDePago.NOMBRE,editFormasDePago.nombre);


        String whereClause=String.format("%s=? ",
                FormasDePago.ID);
        String[] whereArgs={editFormasDePago.idFormaPago};

        int resultado=db.update(Tablas.FORMA_PAGO,valores,whereClause,whereArgs);

        return resultado>0;
    }

    //@DELETE FormasDePago
    public boolean eliminarFormasDePago(String idFormasDePago){


        SQLiteDatabase db= baseDatos.getWritableDatabase();


        String whereClause=String.format("%s=? ",
                FormasDePago.ID);
        String[] whereArgs={idFormasDePago};

        int resultado=db.delete(Tablas.FORMA_PAGO,whereClause,whereArgs);

        return resultado>0;
    }

    public SQLiteDatabase getDb() {
        return baseDatos.getWritableDatabase();
    }



}
