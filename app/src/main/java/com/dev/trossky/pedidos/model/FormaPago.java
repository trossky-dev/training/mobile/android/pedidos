package com.dev.trossky.pedidos.model;

/**
 * Created by luis on 30/09/17.
 */

public class FormaPago {
    public String idFormaPago;
    public String nombre;

    public FormaPago(String idFormaPago, String nombre) {
        this.idFormaPago = idFormaPago;
        this.nombre = nombre;
    }
}
