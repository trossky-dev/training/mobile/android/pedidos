package com.dev.trossky.pedidos;

import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.dev.trossky.pedidos.model.CabeceraPedido;
import com.dev.trossky.pedidos.model.Cliente;
import com.dev.trossky.pedidos.model.DetallePedido;
import com.dev.trossky.pedidos.model.FormaPago;
import com.dev.trossky.pedidos.model.Producto;
import com.dev.trossky.pedidos.sqlite.OperacionesBaseDatos;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    OperacionesBaseDatos datos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getApplicationContext().deleteDatabase("pedidos.db");
        datos=OperacionesBaseDatos.obtenerInstancia(getApplicationContext());

        new TareaPruebaDatos().execute();
    }



    public  class TareaPruebaDatos extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            //[POST]
            String fechaActual= Calendar.getInstance().getTime().toString();

            try {
                datos.getDb().beginTransaction();//Start

                //Insercion Clientes
                String cliente1=datos.insertatCliente(new Cliente(null,"Mathias","Garcia","32229534"));
                String cliente2=datos.insertatCliente(new Cliente(null,"Freddy Alexander","Garcia","310752015"));

                //Insercion Formas de Pago
                String formapago1=datos.insertatFormasDePago(new FormaPago(null,"Efectivo"));
                String formapago2=datos.insertatFormasDePago(new FormaPago(null,"Credito"));

                //insersion Producto
                String producto1=datos.insertatProducto(new Producto(null,"Manzana Unidad",2f,100));
                String producto2=datos.insertatProducto(new Producto(null,"Pera Unidad",3f,230));
                String producto3=datos.insertatProducto(new Producto(null,"Guayaba Unidad",5f,55));
                String producto4=datos.insertatProducto(new Producto(null,"Mani Unidad",3.6f,60));

                //insercion Pedido
                String pedido1=datos.insertatCabeceraPedido(new CabeceraPedido(null,fechaActual,cliente1,formapago1));
                String pedido2=datos.insertatCabeceraPedido(new CabeceraPedido(null,fechaActual,cliente2,formapago2));

                //insercion Detalle Pedido
                datos.insertatDetallePedido(new DetallePedido(pedido1,1,producto1,5, 2f));
                datos.insertatDetallePedido(new DetallePedido(pedido1,2,producto2,10, new Float(3)));
                datos.insertatDetallePedido(new DetallePedido(pedido2,1,producto3,30, new Float(5)));
                datos.insertatDetallePedido(new DetallePedido(pedido2,2,producto4,20, 3.6f));

                // Eliminar Detalle
                datos.eliminarCabeceraPedido(pedido1);


                //Actualizar CLiente
                datos.actualizarCliente(new Cliente(cliente2,"Junior","Garcia","7785421"));



                datos.getDb().setTransactionSuccessful();//end
            }catch (Exception e){
                datos.getDb().endTransaction();
            }

            // [QUERIES]
            /**
             * La clase DatabaseUtils es para concatenar sentencias, ligar parámetros a comandos,
             * construir condiciones para la cláusula WHERE, etc. En mi caso usé le método
             * dumbCursor() para mostrar de forma presentable las filas de los cursores que
             * vaya a loguear.
             * */
            Log.d("Clientes","Clientes");
            DatabaseUtils.dumpCursor(datos.obtenerCliente());
            Log.d("Formas de Pago","Formas de Pago");
            DatabaseUtils.dumpCursor(datos.obtenerFormaPago());
            Log.d("Producto","Producto");
            DatabaseUtils.dumpCursor(datos.obtenerProducto());
            Log.d("Pedido","Pedido");
            DatabaseUtils.dumpCursor(datos.obtenerCabeceraPedido());
            Log.d("Detalle Pedido","Detalle Pedido");
            DatabaseUtils.dumpCursor(datos.obtenerDetallePedido());


            return null;
        }


    }


}
