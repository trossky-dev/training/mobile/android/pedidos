package com.dev.trossky.pedidos.model;

/**
 * Created by luis on 30/09/17.
 */

public class Cliente {

    public String idCliente;
    public String nombres;
    public String apellidos;
    public String telefono;

    public Cliente(String idCliente, String nombres, String apellidos, String telefono) {
        this.idCliente = idCliente;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
    }
}
