package com.dev.trossky.pedidos.model;

/**
 * Created by luis on 30/09/17.
 */

public class DetallePedido {
    public String idCabeceraPedido;
    public Integer secuencia;
    public String idProducto;
    public Integer cantidad;
    public Float precio;

    public DetallePedido(String idCabeceraPedido, Integer secuencia, String idProducto, Integer cantidad, Float precio) {
        this.idCabeceraPedido = idCabeceraPedido;
        this.secuencia = secuencia;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }
}
